# Rules for [Computer Science Educators (CSEd) Chat Room][2]

Welcome to the [Computer Science Educators (CSEd) Chat Room][2]!

This is a room where we hang out to talk about how to make the site a
better place, moderation issues, advertising/promotion, and other
_really important_ things. Our main purpose isn't to answer comp sci
or teaching questions (that's what CSEd main is for), but we try to
help when we can.

## General behavior

* Be good to each other, as explained in the [Stack Exchange Be Nice
Policy][3] and the [No Code of Conduct (NCoC)][1] (warning: some language)

* Please keep language safe for work

* Please don't use @username notification unless you are already in a
  conversation with that person.

* We believe that the CSEd chatroom should be a friendly and welcoming
  place.  If you see misbehavior in the room, please @contact any of the
  follwoing people and we'll do our best to sort it out:

  * thesecretmaster

  * BenI.

  * Aurora001

  * GypsySpellweaver

## Asking Questions

* You don't need to ask if anyone is around.  Just ask your question
  and someone will help if they are able.

* You may link to questions on CSEd.  If someone knows the answer, they
  will help.

* If nobody answers, please don't repeat your request.

* You may post short snippets of code into the room.  If it's big
  (say, over a dozen lines) please post a link to the code instead.

* Please format code appropriately: paste your text and indent it with
  control-K.  Or you can press the "fixed font" button that appears
  next to "send" and "upload" when your post is more than one line
  line.

* Don't star posts to reward users for answering your question.

## Unacceptable Topics

* Don't promote illegal activities, including piracy of software or
  books.

[1]: https://github.com/domgetter/NCoC/blob/master/README.md
[2]: https://chat.stackexchange.com/rooms/59174
[3]: https://meta.stackexchange.com/questions/240839/the-new-new-be-nice-policy-code-of-conduct-updated-with-your-feedback
